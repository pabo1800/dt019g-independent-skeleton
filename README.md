#Template repo for DT019G

This repo has now been modified to contain only a single project. This makes
it easier to maintain since any changes only needs to be applied to a single
project. It also makes it useful for other things and not just the course
assignments. Simply create a fork for each of your projects and configure it to
suit your needs.

## Use

 + Simply fork this repo, and clone the fork with CLion or manually. If
   you clone it directly and still want to use your bitbucket repo as the remote
   repo you have to change the remote repo. That is easy enough, but a fork does
   what you want and i easier!
 + There are some important sections in the `CMakeLists.txt` file. They have
   been framed to make them easy to find. The sections are things that differ
   from each project and include project name, source files needed to compile
   and the files that should be included in the zip archive.
 + Header files in the `include/` directories will be detected automatically so
   just include them as usual, without the path. I.e to include a header file
   located in the `include` directory for one of the assignments:

```cpp
// src/mylib.cpp

// To include include/mylib.h
#include "mylib.h
```

If you use cmake to compile it should detect new source files and rebuild the
project.

##Create zip-files automatically

There is a custom build target that will create a zip-file. To run this target
simply select the `zip` target from the build/rung configuration list in CLion
and hit the "hammer" to the left of the dropdown list. This will create a
zip-file with the necessary files and place the file in the `zip` directory
in the root directory.

This can be done manually in the terminal as well:
```
build/$ cmake --build . --target zip
```

##Notes about adding new files to cmake

I have not used automatic detection of files in the build scripts, instead I
have chosen to add each new files explicitly to the executable target. This has
the benefit of letting `CMAKE` detect the changes to the `CMakeLists.txt` file
and automatically reload the build scipt before compiling, when a new file is
added. It also makes it easier to create separate executables using different
source files if needed.

The drawback is that we have to manually insert the path to any new source files
that we add to one of the executables. But it is a simple matter of open up the
`CMakeLists.txt` file and add them below the line `#Add any new source files
here...` so it shouldn't be too hard. All header files `*.h` in the `include`
directory will be added automatically.

##Testing with catch2

There is also a separate branch where I have added testing functionality for
each assignment, using Catch2.

+ [Video presentation](https://www.youtube.com/watch?v=Ob5_XZrFQH0)
+ [Tutorial and reference](https://github.com/catchorg/Catch2).

Testing is outside the requirements for this course. But it is also very useful,
and something that interests me so I thought I would start learning how to do it
in C++. I figured I might as well upload this branch too, in case anyone else
shares this interest.

Both branches have been tested in the terminal using `git`, `cmake` (3.13) and
in CLion. I have not tested it in Visual Studio.

If you want to switch to the testing branch it is easily done in either the
terminal:

```
$ git checkout with-tests
```

or in CLion:

 + Click on `Git:master` in the lower right corner
 + Under `remote branches` select `with-tests` and `import as..`
 + Click `Ok` (default name will be fine)

Run the tests from by choosing the corresponding target in the dropdown list as
any other target. Or from within the build directory in the terminal:

```
build$ ctest --output-on-failure -V
```

## Contributions and feedback

If you have any suggestions or found any problems, either commit the changes to
your fork, push the changes to bitbucket and use it to create a pull request or,
if you are unsure about how the issue can be resolved, create an issue
here or message me in any of the Discord or Ryver channels.
